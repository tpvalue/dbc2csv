**dbc2csv** é uma ferramenta que converte arquivos no formato **dbc** para **csv**. O objetivo é facilitar pesquisas nas bases públicas do DATASUS por meio da conversão dos dados em formato dbc para csv.

*NOTA:* O arquivo **dbc** em questão é um formato proprietário do Departamento de Informatica do SUS (DATASUS) e não possui relação com o formato de mesmo nome da Microsoft FoxPro ou CANdb.

---

## Requisitos de software:
- git
- virtualenv
- blast-dbf

## Instalação do blast-dbf:
```bash
$ git clone https://github.com/eaglebh/blast-dbf.git 
$ cd blast-dbf && make
$ cp blast-dbf /usr/bin/
```

## Instalação dbc2csv:
```bash
$ virtualenv envdbc2csv
$ cd envdbc2csv
$ source envdbc2csv/bin/activate
$ git clone https://tpvalue@bitbucket.org/tpvalue/dbc2csv.git
$ cd dbc2csv
$ pip install -r requirements.txt
```
---
## USO:
1. Ative o ambiente: `$ source envdbc2csv/bin/activate`
2. Acesse dbc2csv: `$ cd envdbc2csv/dbc2csv`
3. Coloque os arquivos .dbc dentro do diretório **data** (envdbc2csv/dbc2csv/data)
4. Execute a conversão `$ python dbf2csv.py` Os arquivos convertidos ficam no diretório **csv** (envdbc2csv/dbc2csv/data/csv)
